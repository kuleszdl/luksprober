#!/usr/bin/python3

import os

    
defaultDumpFileName = 'test.dump'
defaultTmpFileName = '/tmp/key.temp'
defaultLuksDevice = '/dev/sdb'

def buildDDCall(dumpFileName, tmpFileName, numBytesToRead, numBytesToSkip):
    command = 'dd if=' + dumpFileName + ' of=' + tmpFileName + ' bs=1 count=' + str(numBytesToRead) + ' skip=' + str(numBytesToSkip) + ' status=noxfer'
    return command

def process():
    print('Stating processing...\n')
    
    keySizes = [ 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192 ]
    for keySize in keySizes:
        print('\nNow using keysize of ' + str(keySize) + ' bytes...')
        print('****************************************************')
        for skipArgument in range(1, 500):
            
            # Extract the current encryption key candidate
            ddCommand = buildDDCall(defaultDumpFileName, defaultTmpFileName, keySize, skipArgument)
            print('\nDEBUG: Calling dd as follows: ' + ddCommand)
            returned_value = os.system(ddCommand)
            if (returned_value != 0):
                print('ERROR: dd command failed, bailing out!')
                exit(-1)
                
            # Check if device can be opened using the encryption key candidate
            cryptsetupCommand = 'cryptsetup luksOpen --key-file ' + defaultTmpFileName + ' --test-passphrase ' + defaultLuksDevice
            print('DEBUG: Calling cryptsetup command...')
            returned_value = os.system(cryptsetupCommand)
            if (returned_value == 0):
                print('Got ya!')
                exit(0)
            elif (returned_value == 512):
                print('Debug: Key was not accepted.')
            else:
                print('ERROR: cryptsetup command failed unexpectingly, bailing out!')
                exit(-1)
    print('Sorry to say, but we finished without finding a working key. :-(')
                
def main():
    process()
    
if __name__ == "__main__":
    main()
