# luksprober

Simple probing tool for luks keys. It that assumes you have stored a LUKS key somewhere in a file but don't remember where exactly and what keysize you used. It extracts possible candidate keys and tries them, one after another.

WARNING: This software is pre-alpha and uses hardcoded values! Adapt the program to use other other values.

(c) 2020 Daniel Kulesz

luksprober is available under the terms of the GPLv3 license. See LICENSE for details.
